# Production du HTML

L'édition du livre est terminée. Nous allons pouvoir procéder aux premières conversions.

Il est probable qu'une fois les conversions effectuées, il soit nécessaire de revenir à l'édition Markdown pour améliorer les exports. C'est pour cela qu'un export HTML nous est utile pour contrôler que le corps de texte du document soit correctement édité.

Vous pouvez dans un premier temps lancer un export html (si vous avez une bibliographie, ajoutez l'option `--bibliography=references.bib`) :

      pandoc --standalone --ascii -f markdown -t html monfichier.md monfichier.yaml -o monfichier.html

Pour une lecture plus agréable, vous pouvez ajouter le fichier [pandoc.css](/ressources/pandoc.css) :

      pandoc --standalone --ascii --css="ressources/pandoc.css"  -f markdown -t html monfichier.md monfichier.yaml -o monfichier.html

Vous pouvez maintenant :
  - contrôler que les différentes parties et éléments du texte soient correctement édité,
  - éventuellement corriger les sources
  - relancer la commande précédente pour finaliser la sortie HTML.

## Pour aller plus loin

L'édition web d'un ouvrage peut constituer un site web en tant que tel. Pour aller plus loin, un exercice consiste à diviser le html précédemment produit en plusieurs pages HTML qui constitueront ensemble un site web complet.

Pour cela, il peut être utile d'utiliser un template css préexistant (voir [W3.css](https://www.w3schools.com/w3css/default.asp) ou [Bootstrap](https://www.w3schools.com/bootstrap/bootstrap_templates.asp)), et de le décliner pour produire une navigation entre chapitre.

Pour faciliter la production du site, on peut par exemple :
  - adapter le template choisi comme un template Pandoc, de manière à produire automatiquement les différentes pages du site à partir des chapitres,
  - autre méthode : décliner à la main la structure du site, et y intégrer les contenus HTML produits  avec une commande Pandoc sans l'option `--standalone`.
