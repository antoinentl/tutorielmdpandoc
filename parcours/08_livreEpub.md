# Production de l'Epub

Comme précédemment, on peut envisager plusieurs workflow selon vos besoins :

1. pandoc md to epub par défaut, puis on retouche l'epub
2. pandoc tex to epub

Quelques options :

`--epub-chapter-level=NUMBER`

>    Specify the header level at which to split the EPUB into separate “chapter” files. The default is to split into chapters at level 1 headers. This option only affects the internal composition of the EPUB, not the way chapters and sections are displayed to users. Some readers may be slow if the chapter files are too large, so for large documents with few level 1 headers, one might want to use a chapter level of 2 or 3.

`--epub-cover-image=FILE`

>    Use the specified image as the EPUB cover. It is recommended that the image be less than 1000px in width and height. Note that in a Markdown source document you can also specify cover-image in a YAML metadata block (see EPUB Metadata, below).

Quelques liens utiles dans le manuel Pandoc :

- [Creating EPUBs with pandoc](http://pandoc.org/MANUAL.html#creating-epubs-with-pandoc)
- [Options affecting specific writers](http://pandoc.org/MANUAL.html#options-affecting-specific-writers)
- [Variables set by pandoc](http://pandoc.org/MANUAL.html#variables-set-by-pandoc)

et pleins de petites choses cachées :
- [Manuel Pandoc](http://pandoc.org/MANUAL.html) **+ CTRL+F "epub"** ...
