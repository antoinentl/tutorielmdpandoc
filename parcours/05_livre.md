# Initialisation de l'ouvrage

Nous allons préparer vos sources pour une édition en Markdown.

Dans votre répertoire de travail, créer un sous-dossier `ouvrage` et déplacez y votre.vos fichier.s source.s.

En ligne de commande :

      mkdir ouvrage
      cd ouvrage
      cp /chemin/sources/monfichier.docx ./

Selon votre format d'entrée (docx, odt, html, autre ?), lancer la commande Pandoc pour convertir votre document en Markdown, en remplaçant `docx` par votre format :

      pandoc -f docx -t markdown --extract-media=./ --atx-headers monfichier.docx -o monfichier.md

Vous pouvez maintenant :
1. éditer votre contenu en Markdown
2. produire une bibliographie structurée en bibtex
3. préparer les métadonnées génériques dans un fichier séparé (`monfichier.yaml`)


## Cas particulier

### Format d'entrée non reconnu (Pdf)
Certains formats ne sont pas reconnus en entrée par Pandoc, comme le Pdf. Dans ce cas, utiliser un convertisseur en ligne (vous pouvez faire une requête "pdf 2 text" dans votre moteur de recherche).

### Encodage du texte

Votre document en entrée n'est pas nécessairement encodé en UTF-8. Dans ce cas, commencez par convertir votre document source dans le format d'encodage UTF-8.

_Pour en savoir plus sur l'encodage numérique du texte : [Les formats](http://parcoursnumeriques-pum.ca/les-formats) de Alain Mill, dans «Pratiques de l'édition numérique»_

1. identifiez le format d'encodage de votre document source. Ce format est souvent dans les métadonnées du document. En ouvrant le document dans Atom, Atom devrait vous indiquer le format du document.
2. Une fois le format source identifié (ici iso-8859-1), vous pouvez lancer la commande :

      iconv -f iso-8859-1 -t utf-8 monfichier.html -o monfichier-utf8.html

Cela créera un document source intermédiaire au même format (ici Html), encodé en UTF-8. C'est ce dernier que vous pourrez convertir en Markdown.

## Regex
