# Un peu de théorie

## Gestion de fichiers

### L'arborescence des fichiers

Il faut d'abord comprendre comment est organisé votre ordinateur et où se trouvent vos fichiers.

Votre disque dur peut être compris comme une grande boîte dans laquelle se trouvent d'autres boîtes (dossiers).

La boîte principale est ce qu'on appelle "racine". Dans un ordinateur Unix (Mac ou Linux) la racine est `/`. Dans un ordinateur Windows, la racine est `C:/`.

Dans la racine vous trouvez plusieurs "boîtes".

![arborescence](https://image.jimcdn.com/app/cms/image/transf/dimension=1920x400:format=jpg/path/s2bf1aa8287716b07/image/ic069251148aebd24/version/1522921075/image.jpg)

L'emplacement du Dossier correspond à ce qu'on appelle son "chemin" (_path_).

Par exemple, par rapport à l'image ci-dessus, le dossier "Vacances" aura le chemin `/Document Personnel/Images/Vacances (dans un système Unix)` et `C:/Dossier Personnel/Images/Vacances`

Il est important que vous connaissiez la structure de l'arborescence de votre ordinateur car c'est ainsi qu'on accède aux fichiers.

### Nomenclature de fichier

_Quel nom donner à un fichier ?_ Une question pas si banale que ça. Il existe des règles qui sont fondamentales pour :
  - classer un fichier,
  - le retrouver facilement,
  - partager ses fichiers avec d'autres personnes
  - éviter des problèmes de comptabilité d'un système à un autre

Quelques règles à appliquer :

1. **les noms de fichier ne doivent pas contenir des caractères spéciaux, des accents NI des espaces.** Ce types de caractères peuvent poser des problèmes car l’ordinateur ne les reconnais pas immédiatement, il doit les convertir et dans la conversion il y a souvent des erreurs. Le fichier `mémoire de maîtrise.doc` sera difficile à retrouver. On préférera `memoire_de_maitrise.doc`.
2. **les noms des fichiers doivent être significatifs.** Le fichier `chapitre1.md` ne précise pas de quel ouvrage il est le chapitre 1. On préférera `Espace-numerique_chapitre1.md`.
3. **faites attention aux extensions que vous utilisez.** Par principe, ne masquez pas les extensions de fichiers dans vos navigateurs de fichiers. L'extension est une information primordiale pour la machine et les utilisateurs. Elle renseigne sur le format du contenu du fichier et sur ce qu'on doit/peut faire avec ce fichier. Un fichier `.doc` ne contient pas les mêmes données qu'un fichier `.pdf` par exemple.

Récapitulatif :

- pas de caractères spéciaux
- pas d'espaces
- extension
- nom parlant
- camelCase
- exemples : `2019-01-11notesEDN6102marcelloVitaliRosati.md`

[pour aller plus loin](http://ptaff.ca/blogue/2008/06/29/les_3_rgles_dor_pour_nommer_un_fichier/).

### Versionnage


## Formats de document

Le format d'un document déclare les principes de structuration du document.

On distinguera deux catégories de formats :

1. **Formats propriétaires** : doc, docx, pdf, indd, rtf, kindle, etc.
    - appartiennent à des entités privés
    - conditionnent les pratiques
    - posent des problèmes de perennité
    - exigences commerciales avant les exigences de recherche
    - peuvent être ouverts (exemple du format pdf)

2. **Formats libres** : txt, HTML, XML, TeX, OpenDocument, EPUB, Markdown, etc.
    - appartiennent à la communauté, ce qui favorise la perennité
    - sont standards, ce qui favorise la perennité
    - sont rédéfinis par les pratiques
    - exigences de recherche en premier !

## Chaînes éditoriales

À nouveau, on distingue :

- des chaines basées sur des formats propriétaires : de [Microsoft Word](https://fr.wikipedia.org/wiki/Microsoft_Word) à [Adobe InDesign](https://fr.wikipedia.org/wiki/Adobe_InDesign) par exemple.
- des chaines basées sur des formats libres et standards : [XML](https://fr.wikipedia.org/wiki/Extensible_Markup_Language), [TeX](https://fr.wikipedia.org/wiki/TeX), [Markdown](https://fr.wikipedia.org/wiki/Markdown), etc.

[Markdown](https://daringfireball.net/projects/markdown) (`.md`) est un format de plus en plus utilisé comme format pivot dans les chaines de publication car il permet une édition structurée très simple capable de produire à peu près tous les formats.

Remarque: ce cours est édité en Markdown (cliquez sur ![display source](./media/displaySource.png) pour voir la source).

Getty, O'Reilly sont deux exemples de chaines de publication basées sur des formats de markup simple (respectivement Markdown et AsciiDoc). Pour en savoir plus, voir [le mémoire d'Antoine Fauchié](http://memoire.quaternum.net/).

Dans la prochaine partie, nous expérimenterons la chaîne _Stylo_, basée sur Markdown comme format pivot.


## Edition savante

Exigences particulières du secteur de l'édition savante.

Enjeu principal de l'édition savante, c'est la structuration du document :
- choisir des formats qui permettent d'expliciter précisément la fonction de chaque élément du document (titre, citation, métadonnées, références bibliographiques, etc.)

Chaine markdown particulièrement adapté pour des contenus basés sur le texte.


### Contenus et Métadonnées

### Bibliographie

Quelques ressources :
- [Qu'est-ce que Zotéro ?](http://editorialisation.org/ediwiki/index.php?title=Zotero)
- [Comment installer et utiliser Zotéro ?](http://www.bib.umontreal.ca/lgb/Zotero/tutoriel/1-installer-zotero.htm)
- [Comment importer rapidement une bibliographie vers Zotéro ?](https://www.youtube.com/watch?v=MbrnQVr5ElE)

---
Voir la suite [[02_stylo](./02_stylo.md)]
